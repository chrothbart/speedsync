# Todo:
# implement += and -= for speed curve)

# Libraries
require_relative "lib/nce/usb"
require_relative "lib/nce/bachrus"

# Constants
PRG_TRK_PORT = '/dev/ttyS6'
SPED_TRK_PORT = '5'
MANUF_ID = { 11 => 'NCE', 141 => 'Soundtraxx', 151 => 'ESU' }

# Program variables
loco_cvs = {}

# Open program track port
prgTrk = NCE::USB.new()
prgTrk.port = PRG_TRK_PORT

if prgTrk.open_port
  p "Program Track Open"

# Open speedo
  speedo = Bachrus::Speedo.new SPED_TRK_PORT

# Enter program track mode
  prgTrk.enter_program_mode

# Verify decoder make/model is supported
  loco_cvs[8] = prgTrk.read_cv( 8 )
  raise unless MANUF_ID.has_key? loco_cvs[8]
  p "Programming #{MANUF_ID[loco_cvs[8]}"
#
#   Read loco config
#     Parse loco address type
#     Parse whether speed table is enabled
#
#   Read loco address (long/short based on address type)
#
#   Set and verify speed table bit -- can I set using OPS programming when in program mode? (would verify ops mode/address works)
#
# Close programming track
# 
# Speedmatch loco
#   Create straight speed table
#   Program table to loco
#
#   Command loco to full speed (speed step 126)
#
#   Read till speed stablises and get max speed
#   Update speed table with linear curve
#
#   Program mid speed index
#     Command loco to half of full speed
#     Read till speed stabilizes
#     If speed is outside tollerance, add/sub to curve index by AJUSTMENT_VALUE
#     Build curve above and below value
#     Program curve to loco
#     Repeat until speed in tollerance
#
#   Program 1/4 & 3/4 speeds?
#
#   Verify all speeds between index 1 and max speed
#     For each speed
#       Command loco to correct speed
#       Read till speed stabilizes
#       If speed is outside tollerance, add/sub to curve index by ADJUSTMENT_VALUE
#       Program index of curve
#       Repeat until speed in tollerance
#
# Hope all this works
#
#
prgTrk.close
p "LOL"
