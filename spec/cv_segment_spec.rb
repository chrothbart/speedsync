require 'rspec'

RSpec.describe 'CVSegment' do

  context '.new' do

    describe 'with minimum params' do
      let(:cv) do
        FactoryBot.build(:cv_segment)
      end

      it 'should have a name' do
        expect(cv.name).to eq 'TestName'
      end

      it 'should have the correct bit_range' do
        expect(cv.bit_range).to eq 5..5
      end

      it 'should have the correct value_range' do
        expect(cv.value_range).to eq 0..1
      end

      it 'should have no default value' do
        expect(cv.default_value).to be_nil
      end

      it 'should have the correct address' do
        expect(cv.address).to eq '1.5'
      end
    end

    context 'with invalid param' do
      it 'should validate address' do
        expect{FactoryBot.build(:cv_segmented, address: 0)}.to raise_error('Invalid CV Address')
        expect{FactoryBot.build(:cv_segmented, address: 9000)}.to raise_error('Invalid CV Address')
      end
      [
          {
              params: [
                  :bit_low, :bit_high, :bit
              ],
              low_value: -1,
              high_value: 10
          },
          {
              params: [
                  :val_min, :val_max
              ],
              low_value: -1,
              high_value: 256
          }
      ].each do |test|
        test[:params].each do |param|
          it "should validate #{param}" do
            expect{FactoryBot.build(:cv_segmented, param => test[:low_value])}.to raise_error("Invalid CV #{param}")
            expect{FactoryBot.build(:cv_segmented, param => test[:high_value])}.to raise_error("Invalid CV #{param}")
          end
        end
      end
      it 'should validate default' do
        expect{FactoryBot.build(:cv_segmented, default: -1)}.to raise_error('Invalid CV default')
        expect{FactoryBot.build(:cv_segmented, default: 256)}.to raise_error('Invalid CV default')
      end
      it 'should require segments' do
        expect{FactoryBot.build(:cv_segmented, segments: nil)}.to raise_error('Segments required for segmented CV')
      end
    end
  end

  context '.value=' do
    it 'should set value [bit]' do
      cv = FactoryBot.build(:cv_segment, value: 1)
      expect(cv.value).to eq 1
    end
    it 'should set value [true]' do
      cv = FactoryBot.build(:cv_segment, value: true)
      expect(cv.value).to eq 1
    end
    it 'should set value [false]' do
      cv = FactoryBot.build(:cv_segment, value: false)
      expect(cv.value).to eq 0
    end
    it 'should raise if out of range' do
      expect{FactoryBot.build(:cv_segment, value: 2)}.to raise_error 'New value outside range'
    end
    it 'should raise for "true" on non-bit cv' do
      expect{FactoryBot.build(:cv_segment, bit_low: 0, bit_high: 3, value: true)}.to raise_error 'Non-integer provided as value for multi-bit CV'
    end
  end

  context '.value?' do
    it 'should return true' do
      cv = FactoryBot.build(:cv_segment, value: 1)
      expect(cv.value?).to eq true
    end
    it 'should return false' do
      cv = FactoryBot.build(:cv_segment, value: 0)
      expect(cv.value?).to eq false
    end
  end

  context '.byte_value' do
    it 'should return correct value' do
      cv = FactoryBot.build(:cv_segment, value: true)
      expect(cv.byte_value).to eq 32
    end
  end
  context 'single bit' do
    8.times do |n|

      it "should have a value range of 0..1" do
        cv = FactoryBot.build(:cv_segment, bit: n)
        expect(cv.value_range).to eq 0..1
      end

      context ".byte_value" do
        it "when cv is true" do
          cv = FactoryBot.build(:cv_segment, bit: n, value: true)
          expect(cv.byte_value).to eq (1 << n)
        end

        it "when cv is false" do
          cv = FactoryBot.build(:cv_segment, bit: n, value: false)
          expect(cv.byte_value).to eq (0)
        end
      end
    end
  end
end
