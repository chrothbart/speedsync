

require_relative '../lib/cv/cv_list'

FactoryBot.define do
  factory :cv, class: CV do
    name { "TestName" }
    address { 1 }
    initialize_with { new(attributes) }
  end
  factory :cv_segmented, class: CVSegmented do
    name { "TestName" }
    address { 1 }
    segments { [
      FactoryBot.attributes_for(:cv_segment, parent: self)
    ] }
    initialize_with { new(attributes) }
  end
  factory :cv_segmented_full, class: CVSegmented do
    name { "TestName" }
    address { 1 }
    segments { [
        { name: 'foo', bit: 0, value: true},
        { name: 'bar', bit: 3, value: true},
        { name: 'baz', bit_low: 5, bit_high: 7, value: 5}
    ] }
    initialize_with { new(attributes) }
  end
  factory :cv_segmented_mixed, class: CVSegmented do
    name { "TestName" }
    address { 1 }
    segments { [
        { name: 'foo', bit_high: 6 },
        { name: 'bar', bit: 7 },
    ] }
    initialize_with { new(attributes) }
  end
  factory :cv_segment, class: CVSegment do
    name { "TestName" }
    bit { 5 }
    parent_cv do
      FactoryBot.build(:cv_segmented, segments: [])
    end
    initialize_with { new(attributes) }
  end
end