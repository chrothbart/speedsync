require 'rspec'

RSpec.describe 'CVSegmented' do

  context '.new' do
    describe 'with minimum params' do
      let(:cv) do
        FactoryBot.build(:cv_segmented)
      end

      it 'should have a name' do
        expect(cv.name).to eq 'TestName'
      end

      it 'should default to bit_range 0..7' do
        expect(cv.bit_range).to eq 0..7
      end

      it 'should default to val_range 0..255' do
        expect(cv.value_range).to eq 0..255
      end

      it 'should have no default value' do
        expect(cv.default_value).to be_nil
      end
    end
    context 'with invalid param' do
      it 'should validate address' do
        expect{FactoryBot.build(:cv, address: 0)}.to raise_error('Invalid CV Address')
        expect{FactoryBot.build(:cv, address: 9000)}.to raise_error('Invalid CV Address')
      end
      [
          {
              params: [
                  :bit_low, :bit_high, :bit
              ],
              low_value: -1,
              high_value: 10
          },
          {
              params: [
                  :val_min, :val_max
              ],
              low_value: -1,
              high_value: 256
          }
      ].each do |test|
        test[:params].each do |param|
          it "should validate #{param}" do
            expect{FactoryBot.build(:cv, param => test[:low_value])}.to raise_error("Invalid CV #{param}")
            expect{FactoryBot.build(:cv, param => test[:high_value])}.to raise_error("Invalid CV #{param}")
          end
        end
      end
      it 'should validate default' do
        expect{FactoryBot.build(:cv, default: -1)}.to raise_error('Invalid CV default')
        expect{FactoryBot.build(:cv, default: 256)}.to raise_error('Invalid CV default')
      end
    end
  end

  context '.value' do
    it 'should return calculated value' do
      @cv = FactoryBot.build(:cv_segmented_full)
      expect(@cv.value).to eq 169
    end
    it 'should handle all-zeros' do
      @cv = FactoryBot.build(:cv_segmented, segments: [{ name: 'foo', bit: 0, value: false }])
      expect(@cv.value).to eq 0
    end
    it 'should handle mixed-values' do
      @cv = FactoryBot.build(:cv_segmented, segments: [{ name: 'foo', bit: 0, value: false }, { name: 'bar', bit: 7, value: true}])
      expect(@cv.value).to eq 128
    end
  end

  context '.value=' do
    it 'should set value' do
      @cv = FactoryBot.build(:cv_segmented_full)
      @cv.value = 104
      expect(@cv.value).to eq 104
    end
    it 'should set mixed segments' do
      @cv = FactoryBot.build(:cv_segmented_mixed)
      @cv.value = 208
      expect(@cv.value).to eq 208
    end
  end

  describe 'single bit' do
    8.times do |n|
      before(:each) do
        @cv = FactoryBot.build(:cv, address: 1, bit: n)
      end

      it "#{n} should have a range of 1" do
        expect(@cv.value_range).to eq 0..1
      end
    end
  end
end
