require 'rspec'

RSpec.describe 'CV' do

  context '.new' do
    describe 'with minimum params' do
      let(:cv) do
        FactoryBot.build(:cv)
      end

      it 'should have a name' do
        expect(cv.name).to eq 'TestName'
      end

      it 'should default to bit_range 0..7' do
        expect(cv.bit_range).to eq 0..7
      end

      it 'should default to val_range 0..255' do
        expect(cv.value_range).to eq 0..255
      end

      it 'should have no default value' do
        expect(cv.default_value).to be_nil
      end
    end
  end

  context '.value' do
    it 'should set value' do
      cv = FactoryBot.build(:cv, value: 15)
      expect(cv.value).to eq 15
    end
  end

  context '.value?' do
    it 'should raise on non-boolean CV' do
      cv = FactoryBot.build(:cv)
      expect{cv.value?}.to raise_error('Boolean operation on non-boolean CV')
    end
    describe 'boolean cv' do
      it 'should raise if value is not set' do
        cv = FactoryBot.build(:cv, bit: 0)
        expect{cv.value?}.to raise_error('Unable to get value of uninitialized CV')
      end
      it 'should return true if value is 1' do
        cv = FactoryBot.build(:cv, bit: 0, value: 1)
        expect(cv.value?).to be true
      end
      it 'should return false if value is 0' do
        cv = FactoryBot.build(:cv, bit: 0, value: 0)
        expect(cv.value?).to be false
      end
    end
  end

  context 'self.create_cv' do
    it 'should create a standard cv' do
      cv = CV.create_cv( FactoryBot.attributes_for(:cv) )
      expect(cv).to be_instance_of CV
    end
    it 'should create a segmented cv' do
      cv = CV.create_cv( FactoryBot.attributes_for(:cv_segmented) )
      expect(cv).to be_instance_of CVSegmented
    end
  end

  context 'with invalid param' do
    it 'should validate address' do
      expect{FactoryBot.build(:cv, address: 0)}.to raise_error('Invalid CV Address')
      expect{FactoryBot.build(:cv, address: 9000)}.to raise_error('Invalid CV Address')
    end
    [
        {
            params: [
                :bit_low, :bit_high, :bit
            ],
            low_value: -1,
            high_value: 10
        },
        {
            params: [
                :val_min, :val_max
            ],
            low_value: -1,
            high_value: 256
        }
    ].each do |test|
      test[:params].each do |param|
        it "should validate #{param}" do
          expect{FactoryBot.build(:cv, param => test[:low_value])}.to raise_error("Invalid CV #{param}")
          expect{FactoryBot.build(:cv, param => test[:high_value])}.to raise_error("Invalid CV #{param}")
        end
      end
    end
    it 'should validate default' do
      expect{FactoryBot.build(:cv, default: -1)}.to raise_error('Invalid CV default')
      expect{FactoryBot.build(:cv, default: 256)}.to raise_error('Invalid CV default')
    end
  end

  describe 'single bit' do
    8.times do |n|
      before(:each) do
        @cv = CV.new( name: 'test', address: 1, bit: n)
      end

      it "#{n} should have a range of 1" do
        expect(@cv.value_range).to eq 0..1
      end
    end
  end
end
