##########
# Name: TBA.rb
# Author: Charles Rothbart
#
# Written: 2017-05-17
# Updated: 2018-03-05

# Libraries
#require_relative 'lib/bachrus/speedo.rb'
require_relative 'lib/nce/usb.rb'

# Constants

DEBUG = true

# Additional Methods

def print_menu( *items )

  puts"\nWhat would you like to do?\n"

  items.each_with_index do |item, index|
    puts "#{index + 1}: #{item}"
  end

  puts "0: Exit"

  print "\n> "

  user_in = gets.chomp
end

def utility_programming( prgTrk )
  if prgTrk.enter_program_mode
    while( true ) do # Utility programming loop
      user_in = print_menu "Read CV", "Write CV"

      case user_in
      when "1"

        puts "Enter CV Number"
        print "\n> "

        cv = gets.chomp

        puts "CV Data: #{prgTrk.read_cv cv}"

      when "2"

        puts "Enter CV Number"
        print "\n> "

        cv = gets.chomp

        puts "Enter Value"
        print "\n> "

        data = gets.chomp

        prgTrk.write_cv cv, data

      when "0"
        puts "Closing program track"
        prgTrk.exit_program_mode
        return
      else
        puts "Invalid Input"
      end
    end # End utility programming loop
  else
    p "Error entering programming mode (is the track shorted?)"
  end
end

def speed_match( prgTrk )
  if prgTrk.enter_program_mode
    decoder_cv = {}

    decoder_cv[:configuration] = prgTrk.read_cv 29

    # Determine address type and read
    if (decoder_cv[:configuration] & 0b00100000) == 0
      # Short address
      loco_address = prgTrk.read_cv 1
    else
      loco_address = get_long_address prgTrk


    # Verify speed table bit is set, set it if not
    if (decoder_cvI[:configuration] & 0b10000) == 0






  end

end

# Begin Program
while( true ) do

  user_in = print_menu( "Program Locomotive" )

  case user_in
  when "1"
    puts "Opening programming track connection"

    prgTrk = NCE::USB.new()
    prgTrk.port = "/dev/ttyS6"
    if prgTrk.open_port
      puts "Program track open"

      while( true ) do
        user_in = print_menu "Speedmatch Locomotive", "Utility Programming"

        case user_in
        when "1"
        when "2"
          utility_programming prgTrk
        when "0"
          break
        else
          puts "Invalid Input"
        end
      end
    else
      raise "Error opening programming track"
    end


  when "0"
    puts "Exiting"
    break
  else
    puts "Invalid Input"
  end
end

