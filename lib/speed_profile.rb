##########
# Name: speed_profile.rb
# Author: Charles Rothbart
#
# Written: 2018-02-25
# Updated: 2018-02-25

class SpeedProfile
  # Constants
  MAX_SCALE_SPEED = 255
  VALID_SPEED_STEPS = 28, 126
  DEFAULT_SPEED_STEPS = 126
  PRINT_ROWS = { 40 => '40', 30 => '30', 25 => ' |', 20 => '21', 15 => ' |', 10 => '10', 9 => ' |', 8 => ' |', 7 => ' |', 6 => ' |', 5 => ' |', 4 => ' |', 3 => ' |', 2 => ' |', 1 => ' |'}

  # Attributes
  attr :speed_map, :speed_range, :speed_steps_range
  attr_accessor :speed_steps

  def initialize( params = {} )
    if params.has_key?( :speed_steps ) && VALID_SPEED_STEPS.include?(params[:speed_steps ] )
      @speed_steps = params[:speed_steps]
    else
      @speed_steps = DEFAULT_SPEED_STEPS
    end

    # Initialize empty array to store speed values
    @speed_map = Array.new( DEFAULT_SPEED_STEPS + 1, 0 ) # Arrays are zero-indexed, adding one keeps SpeedStep alignment

    @speed_range = 1..MAX_SCALE_SPEED
    @speed_steps_range = 1..@speed_steps
  end

  def set_speed( step, speed )
    raise 'Speed step outside range' unless @speed_steps_range === step
    raise 'Speed outside range' unless @speed_range === speed

    @speed_map[step] = speed
  end

  def print_table

    # Calculate Offset
    offsets = []
    @speed_map.each_with_index do |speed, step|
      diff = speed - step
      offsets << diff
    end
    
    # Print positive section of table
    PRINT_ROWS.each do |row, char|
      print char
      offsets.each_with_index do |offset, index|
        if offset >= row
          print '*'
          offsets[index] = 0
        else
          print ' '
        end
      end
      print "\n"
    end

    # Print center spine
    print " 0"
    @speed_steps_range.each do |index|
      print (index == 0 && ' 0') || ((index % 10 == 0) && (index.to_s[-2] )) || '='
    end
    print "\n"

    # Print negative section of table
    PRINT_ROWS.reverse_each do |row, char|
      print char
      offsets.each_with_index do |offset, index|
        if offset >= (row * -1)
          print '*'
          offsets[index] = 0
        else
          print ' '
        end
      end
      print "\n"
    end
  end

  def set_straight_curve( max_speed )
    @speed_map.each_index do |index|
      next if index == 0
      @speed_map[index] = (max_speed*(index.to_f/@speed_steps)).floor
    end
    self
  end

  # Class Methods
  def self.to_mph( step )
    step.to_f * 4.5
  end

  def self.to_step( mph )
    (mph.to_f / 4.5).ceil
  end
end