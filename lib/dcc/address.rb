
module DCC
  class Address

    # Constants
    DECODER_TYPES = [ :loco_short, :loco_long ]
    SHORT_ADDRESS_RANGE = 1..127
    LONG_ADDRESS_RANGE = 1..0x27FF
    LONG_ADDRESS_MASK = 0b1100000000000000

    # Attributes
    attr :type
    attr_accessor :address

    def initialize( params = {} )
      if params.has_key?( :address ) and params.has_key?( :type )
        @address = params[ :address ]
        @type = params[:type]
      end

      raise 'Invalid short address' if (@type == :loco_short) && !self.class.valid_short?( @address )
      raise 'Invalid long address' if (@type == :loco_long) && !self.class.valid_long?( @address )
    end

    def packet_address
      return (LONG_ADDRESS_MASK | @address) if @type == :loco_long

      @address
    end

    # Class Methods

    def self.read_address( interface, parameters = {} )
      config_cv = parameters[:config_cv] || interface.read_cv( 29 )

      if (config_cv & 0b00100000) == 0
        type = :loco_short
        address = interface.read_cv 1
      else
        type = :loco_long
        high = interface.prgTrk.read_cv 17
        low = interface.prgTrk.read_cv 18
        address = (high << 8) + low
      end
      Address.new( address: address, type: type )
    end

    def self.valid_short?( address )
      return true if SHORT_ADDRESS_RANGE === address
      return false
    end

    def self.valid_long?( address )
      return true if LONG_ADDRESS_RANGE === address
      return false
    end
  end
end

