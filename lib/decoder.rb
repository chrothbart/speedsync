##########
# Name: decoder_programmer.rb
# Author: Charles Rothbart
#
# Written: 2018-02-25
# Updated: 2018-02-25

# Libraries
require_relative 'speed_profile'
require_relative 'speed_table'
require_relative 'cv/cv_list'
require_relative 'dcc/address'

class Decoder
  # Constants
  # Attributes
  attr :speed_profile, :cvs, :speed_table

  def initialize( params = {} )
    @speed_profile = SpeedProfile.new
    @cvs = CVList.new
    @speed_table = SpeedTable.new

    if params.has_key? :profile
      @cvs.load_file params[:profile]
    end
  end

  def cv( cv_name )
    @cvs.find_cv cv_name
  end

  def set_cv ( cv_name, new_value )
    @cvs.find_cv( cv_name ).value = new_value
  end

  def set_cvs ( cvs = {} )
    cvs.each { |cv_name, cv_value| self.set_cv( cv_name, cv_value) }
  end

  def load_cvs ( params = {} )
    if params.has_key? :file
      @cvs.load_file params[:file]
    elsif params.has_key? :cvs
      @cvs.load_cvs params[:cvs]
    else
      raise 'Invalid option provided'
    end
  end

  def address_type
    if @cvs.find_cv('Configuration.AddressType').value?
      :loco_long
    else
      :loco_short
    end
  end

  def address
    if address_type == :loco_short
      @cvs.find_cv('PrimaryAddress').value
    elsif address_type == :loco_long
      (@cvs.find_cv('LongAddressHigh').value << 8) + @cvs.find_cv('LongAddressLow').value
    else
      raise 'Invalid address type'
    end
  end

  def get_address
    DCC::Address.new( address: self.address, address_type: self.address_type )
  end
end