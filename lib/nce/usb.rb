##########
# Name: nce/usb.rb
# Author: Charles Rothbart
#
# Written: 2017-07-15
# Updated: 2018-04-05

# Libraries
require 'serialport'
require_relative '../dcc/address'

module NCE
  class USB

    # Constants
    SER_RATE = 9600
    SERIAL_READ_TIMEOUT = 100
    MAXIMUM_READ_TIME = 1000
    ENTER_PROGRAM_MODE_TIMEOUT = 5000

    DEFAULT_READ_TYPE = :direct
    READ_COMMANDS = { paged: 'A1', register: 'A7', direct: 'A9' }
    WRITE_COMMANDS = { paged: 'A0', register: 'A6', direct: 'A8', ops: 'AE' }

    LOCO_COMMANDS_RANGE = 0x01..0x16
    LOCO_COMMANDS = {}

    attr :port_obj, :in_program_mode
    attr_accessor :port_id

    def initialize
      @port = nil
      @port_id = nil
      @in_program_mode = false
    end

    def open_port
      @port_obj = SerialPort.new( @port_id, SER_RATE, 8, 1, SerialPort::NONE)
      
      unless SERIAL_READ_TIMEOUT.nil?
        @port_obj.read_timeout = SERIAL_READ_TIMEOUT
      end

      # Check if connection is working and return
      is_connected?
    end

    def close
      exit_program_mode if @in_program_mode
      @port_obj.close
    end

    def port= (new_port)
      # Todo: Add port number validation
      @port_id = new_port
    end

    def is_connected?
      # Send ping
      send_data "0x80"
      return true if read_data( 1 )

      false
    end

    def enter_program_mode
      return true if @in_program_mode

      send_data "0x9E"
      return @in_program_mode = true if read_data( 1, timeout: ENTER_PROGRAM_MODE_TIMEOUT )

      raise "Unable to enter program mode"
    end

    def exit_program_mode
      return true unless @in_program_mode

      send_data "0x9F"
      return true if read_data( 1, timeout: 5000 )

      raise "Error when exiting program mode"
    end

    # Name: read_cv
    #
    # Reads a loco CV from programming track. Supports all programming modes.
    #
    # Params:
    #   cv: cv number to be read
    #   mode: programming mode
    def read_cv(cv_number, params = {} )
      puts "Read cv: #{cv_number}" # Todo: Remove print statement

      # Defaults
      mode = params[:mode] || DEFAULT_READ_TYPE

      raise "Invalid CV read mode" unless READ_COMMANDS.key? mode
      raise "Attempted to read CV when not in program mode" unless @in_program_mode

      cv_number = cv_number.to_i
      
      send_data sprintf("%s%04x", READ_COMMANDS[mode], cv_number )

      read_data 2, timeout: 5000
    end

    def write_cv( cv_number, data, params = {} )
      mode = params[:mode] || ( params.has_key?(:address) && :ops ) || DEFAULT_READ_TYPE

      raise "Invalid CV write mode" unless WRITE_COMMANDS.key? mode

      cv_number = cv_number.to_i
      data = data.to_i
      address = address_for_ops params[:address].to_i

      if params[:mode] == :ops
        send_data sprintf("%s%04x%04x%02x", WRITE_COMMANDS[mode], address, cv_number, data )
      else
        send_data sprintf("%s%04x%02x", WRITE_COMMANDS[mode], cv_number, data )
      end

      read_data 1
    end

    def control_loco( address, operation, data )
      raise "Invalid Opcode for Loco Control" unless LOCO_COMMANDS_RANGE === operation

      send_data sprintf( "A2%04x%02x%02x", address.packet_address, operation, data )

      read_data 1
    end

    private

      def send_data( data, *options )
        puts "Raw data: #{data}" if DEBUG # Todo: Remove this?

        match_data = /\A(0x)?(([0-9a-fA-F]{2})+)\z/.match( data )

        puts "Send data: #{match_data[2]}"

        if match_data
          hex_data = match_data[2]
        else
          raise "Invalid hex string"
        end

        bytes_written = @port_obj.write( [hex_data].pack("H*") )

        raise "Wrong number of bytes written" unless bytes_written == hex_data.length / 2
      end

      def read_data( bytes, options = {} )
        timeout = ((options.has_key? :timeout) && options[:timeout]) || MAXIMUM_READ_TIME

        puts "Read timeout: #{timeout}" if DEBUG

        data = ""

        end_read_time = Time.now + (timeout / 1000.0)
        while Time.now < end_read_time do
          puts data << @port_obj.read
          break if data.length == bytes
        end

        parse_data data, bytes
      end

      def parse_data( data, bytes )

        puts "Returned data: #{data}" if DEBUG

        data = data.unpack('C*')

        p "Parsed values: #{data}" if DEBUG

        raise "Unexpected data length: #{bytes} expected, received #{data.size}" if data.size != bytes

        case data.last

        when 0x21 # '!'
          return true if data.size == 1
          puts data[0].to_s(2)
          return data[0]
        when 0x30 # '0'
          raise "Command not supported"
        when 0x31 # '1'
          raise "Loco/accy/signal address out of range"
        when 0x32 # '2'
          raise "Cab address or op code out of range"
        when 0x33 # '3'
          raise "CV address or data out of range"
        when 0x34 # '4'
          raise "Byte count out of range"
        else
          raise "Unexpected return code #{data.last.chr}"
        end
      end

      def address_for_ops( address )
        return address.value if address.type == :loco_short
        0b1100000000000000 | address
      end

  end
end
