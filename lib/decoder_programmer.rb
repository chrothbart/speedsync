##########
# Name: decoder_programmer.rb
# Author: Charles Rothbart
#
# Written: 2018-02-25
# Updated: 2018-11-01

require_relative 'nce/usb'
require_relative 'bachrus/speedo'
require_relative 'decoder'

class DecoderProgrammer

  # Constants
  INITIAL_CV_TEMPLATE = 'lib/NMRAStandardProfile.yaml'

  # Attributes
  attr :dcc_interface, :speed_interface, :decoder

  def initialize( params = {} )

    self.set_dcc_interface( params.slice( :dcc_interface_port, :dcc_interface_type))

    self.set_speed_interface( params.slice( :speed_interface_port, :scale))

    @decoder = Decoder.new( profile: INITIAL_CV_TEMPLATE )
  end

  def set_dcc_interface( params = {} )
    if params[:dcc_interface_type] == 'NCE.USB'
      @dcc_interface = NCE::USB.new
      @dcc_interface.port = params[:dcc_interface_port]

      raise 'Unable to open DCC interface' unless @dcc_interface.open_port
    else
      raise 'Unsupported DCC interface type'
    end
  end

# Todo: add scale param
  def set_speed_interface( params = {} )
    if params[:speed_interface_port]
      @speed_interface = Bachrus::Speedo.new params[:speed_interface_port]
    else
      @speed_interface = nil
    end
  end

  def load_decoder_profile

  def read_cv( name )
    @dcc_interface.read_cv(@decoder_current.cv(name).address)
  end

  def cv( name )
    return if decoder.cv( name ).value
  end

  def set_cv( name, value )
    @decoder_pending.set_cv name, value
  end

  def set_cv!( name, value )

  end

  def initalize_from_track

  end

  private

  def write_cv( cv )
    @dcc_interface.write_cv
  end

end

