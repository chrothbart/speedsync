# Libraries
require 'YAML'
require_relative 'cv'
require_relative 'cv_segmented'
require_relative 'cv_segment'

class CVList
  include Enumerable
  # Constants
  CV_NAME_MATCH = /\A(?<cv_name>\w*)(?:.(?<segment_name>\w*))?\z/
  # Attributes
  attr :cv_list

  def initialize( params = {} )

    @cv_list = []

    if params.has_key? :file
      self.load_file params[:file]
    end

    if params.has_key? :cvs
      self.load_cvs params[:cvs]
    end
  end

  def load_file( file )
    self.load_cvs( YAML.load_file file )
  end

  def load_cvs( params = {} )
    params[:cvs].each do |cv|
      cv[:parent_cv] = params[:parent_cv] if params.has_key? [:parent_cv]
      @cv_list << CV.create_cv( cv )
    end
  end

  def to_s
    string = "CV List With #{@cv_list.size} Members\n"
    @cv_list.each {|cv| string << cv.to_s}
    string
  end

  def each(&block)
    @cv_list.each(&block)
  end
  
  def find_cv( name )
    name_match_data = name.match CV_NAME_MATCH
    matched_cvs = @cv_list.select { |cv| cv.name == name_match_data[:cv_name] }
    if name_match_data[:segment_name]
      found_segments = []
      matched_cvs.each do |matched_cv|
        matched_segments = matched_cv.find_segment( name_match_data[:segment_name] )
        found_segments << matched_segments if matched_segments
      end

      raise 'Multiple matching segments found' if found_segments.size > 1

      found_segments.size == 1 && found_segments.first
    else
      raise 'Multiple matching CVs found' if matched_cvs.size > 1
      matched_cvs.size == 1 && matched_cvs.first
    end
  end
end