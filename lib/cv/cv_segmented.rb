require_relative 'cv'

class CVSegmented < CV

  # Attributes
  attr :segment_list

  def initialize( params )
    super

    set_segments( params[:segments] )
  end

  def to_s
    super << @segment_list.to_s
  end

  def value
    calculated_value = 0
    segment_list.each do |segment|
      calculated_value = calculated_value + ( segment.byte_value )
    end
    calculated_value
  end

  def value=(new_value)
    if new_value
      @segment_list.each do |segment|
        segment.byte_value = new_value.to_i
      end
    end
  end

  def find_segment( name )
    @segment_list.find_cv name
  end

  private

  def set_segments( segments )
    raise 'Segments required for segmented CV' unless segments
    @segment_list = CVList.new
#   segments.each do |segment|
#     segment[:parent_cv] = self
#     @segment_list << CVSegment()
      @segment_list = CVList.new( { cvs: segments, parent_cv: self } )
#   end
  end

end
