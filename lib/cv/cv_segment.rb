require_relative 'cv_segmented'

class CVSegment < CV

  # Constants
  CV_ADDRESS_RANGE = 0..7

  # Attributes
  attr :parent_cv

  def initialize( params )
    set_parent( params[:parent_cv] )

    super
  end

  def value?
    if @bit_range.size == 1
      if @value == 1
        true
      else
        false
      end
    else
      @value
    end
  end

  def value=( new_value )
    if new_value != nil
      if /\A\d+\z/.match? new_value.to_s
        super
      elsif bit_range.size == 1
        if new_value.to_s == "true"
          super 1
        elsif new_value.to_s == "false"
          super 0
        end
      else
        raise 'Non-integer provided as value for multi-bit CV'
      end
    end
  end

  def byte_value
    raise 'segment value not set' unless @value
    @value << @bit_range.min
  end

  def byte_value=(new_byte_value)
    new_value = 0
    8.times do |new_value_bit|
      if @bit_range === new_value_bit
        cv_value_bit = new_value_bit - @bit_range.min
        new_value_bit_mask = 1 << new_value_bit
        new_bit_value = (new_value_bit_mask & Integer(new_byte_value)) >> (new_value_bit - cv_value_bit)
#       cv_value_bitmask = 1 << cv_value_bit
#       new_bit_mask = (255 ^ cv_value_bitmask) + new_bit_value
#       @value = @value & new_bit_mask
        new_value = new_value + new_bit_value
      end
    end
    self.value = new_value
  end

  private

  def set_address( params )
    address = params[:bit_low] || params[:bit] || params[:bit_high] && 0
    raise 'Invalid CV Address' unless CV_ADDRESS_RANGE === address
    @address = "#{@parent_cv.address}.#{address}"
  end

  def set_parent( parent )
    raise 'Parent required for CV Segment' unless parent
    # ToDo: validate parent is segmented
    @parent_cv = parent
  end

end
