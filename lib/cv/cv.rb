class CV
  # Constants
  CV_TYPES = [:standard, :constant, :segmented, :segment]
  CV_ADDRESS_RANGE = (1..255)
  MIN_CV_VALUE = 0
  MAX_CV_VALUE = 255
  DEFAULT_CV_VALUE_RANGE = (MIN_CV_VALUE..MAX_CV_VALUE)
  MIN_BIT = 0
  MAX_BIT = 7
  DEFAULT_CV_BIT_RANGE = (MIN_BIT..MAX_BIT)

  # Attributes
  attr :address, :value_range, :bit_range, :default_value
  attr_reader :name, :value

  def initialize( params = {} )

    # Set Name
    set_name params[:name]

    # Set Address
    set_address params.slice( :address, :bit, :bit_low, :bit_high )

    # Set bit range
    self.bit_range = params.slice( :bit_low, :bit_high, :bit )

    # Set value range
    set_value_range params.slice( :val_min, :val_max )

    # Set value
    self.value = params[:value]

    # Set default CV value
    set_default_value params[:default]
  end

  def to_s
"#{@address} #{@name}
  Value: #{@value}
  Default: #{@default_value}
  BitRange: #{@bit_range}
  ValueRange: #{@value_range}
"
  end

  def self.create_cv( cv_params )
    if cv_params.has_key? :segments
      CVSegmented.new cv_params
    elsif cv_params.has_key? :parent_cv
      CVSegment.new cv_params
    else
      CV.new cv_params
    end
  end

  def value=( new_value )
    if new_value
      raise 'New value outside range' unless @value_range === Integer( new_value )
      @value = new_value.to_i
    end
  end

  def value?
    if @value_range == (0..1)
      if @value == 1
        true
      elsif @value == 0
        false
      else
        raise 'Unable to get value of uninitialized CV'
      end
    else
      raise 'Boolean operation on non-boolean CV'
    end
  end

  private

  def set_name( name )
    raise 'Name Required' unless name
    @name = name
  end

  def set_address( params ) #Sets addres
      raise 'CV Address Required' unless params.has_key?(:address)
      raise 'Invalid CV Address' unless CV_ADDRESS_RANGE === params[:address]
      @address = params[:address]
  end

  def bit_range=( params )
    if params.has_key?( :bit_low ) || params.has_key?( :bit_high )
      bit_low = params[:bit_low] || MIN_BIT
      bit_high = params[:bit_high] || MAX_BIT

      raise 'Invalid CV bit_low' unless DEFAULT_CV_BIT_RANGE === bit_low
      raise 'Invalid CV bit_high' unless DEFAULT_CV_BIT_RANGE === bit_high

      @bit_range = (bit_low..bit_high)
    elsif params.has_key?( :bit )
      raise 'Invalid CV bit' unless  DEFAULT_CV_BIT_RANGE === params[:bit]
      @bit_range = params[:bit]..params[:bit]
    else
      @bit_range = DEFAULT_CV_BIT_RANGE
    end
  end

  def set_value_range( params )
    if params.has_key?( :val_min ) || params.has_key?( :val_max )
      val_min = params[:val_min] || MIN_CV_VALUE
      val_max = params[:val_max] || MAX_CV_VALUE

      raise 'Invalid CV val_min' unless DEFAULT_CV_VALUE_RANGE === val_min
      raise 'Invalid CV val_max' unless DEFAULT_CV_VALUE_RANGE === val_max

      @value_range = (val_min..val_max)
    else
      @value_range = MIN_CV_VALUE..( ( 2 ** @bit_range.size ) - 1 )
    end
  end

  def set_default_value( default )
    if default
      raise 'Invalid CV default' unless @value_range === default
      @default_value = default
    else
      @default_value = nil
    end
  end
end
