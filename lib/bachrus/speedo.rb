##########
# Name: BachrusSpeedo.rb
# Author: Charles Rothbart
# Written: 2017-05-16
# Updated: 2018-03-30

# Libraries
require 'serialport'

# Constants

CIRC = 18.8496
SCALE = 87

MILES_PER_KM = 0.621371

module Bachrus
  class Speedo

    attr :port

    def initialize( port_number )
      raise 'Invalid speed interface port' unless /\A\d{1,2}\z/ =~ port_number.to_s

      @port = SerialPort.new("/dev/ttyS#{port_number}", 9600, 8, 1, SerialPort::NONE)
    end

    def print_speed( average_time )
      puts "Speed"
      speeds = []
      times = []

      while true do
        speeds.push( speed_raw || 0 )
        times.push( Time.now )
        if times.first < ( Time.now - average_time.to_i )
          speeds.shift
          times.shift
        end

        print "\r%.1f" % (speeds.inject(:+).to_f / speeds.size)
      end
    end

    def speed_to_file( filename, seconds )
      file = File.open( filename, 'w' )

      run_till = Time.now + seconds.to_i

      while Time.now < run_till do
        speed = speed_avg( 1 )
        print "\r%.1f" % speed
        file.write( sprintf( "%.1f\n" % speed ) )
      end

      file.close
    end

    def speed_avg( seconds )
      run_till = Time.now + seconds.to_i
      speeds = []

      while Time.now < run_till do
        new_speed = speed_raw
        next if new_speed.nil?
        speeds.push new_speed
      end

#     average_speed = speeds.inject(:+).to_f / speeds.size
      speeds.inject(:+).to_f / speeds.size
    end

    def speed_raw
      raw = @port.readline(";")
      if raw.length == 9
        data = raw[2,6]
        data = data.to_i(16)
        freq = 1500000.0 / data
#       speed = ((freq/24) * CIRC * SCALE * (3600.0 / 1000000)) * MILES_PER_KM
        ((freq/24) * CIRC * SCALE * (3600.0 / 1000000)) * MILES_PER_KM
      end
    end
  end
end
