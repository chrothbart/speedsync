##########
# Name: speed_table.rb
# Author: Charles Rothbart
#
# Written: 2018-02-18
# Updated: 2018-02-18

# Libraries

class SpeedTable

  # Constants
  CV_OFFSET = 66
  TOTAL_STEPS = 28

  # Attributes
  attr :table

  def initialize
    @table = Array.new( TOTAL_STEPS, 0)
  end

  def step( number )
    @table[number]
  end

  def set_step(number, value )
    return false unless value.between?(0, 255)

    @table[number] = value.to_i
  end

  def set_curve(params = {} )
    params[:start] = 0 unless params.has_key? :start
    params[:end] = 27 unless params.has_key? :end
    params[:start_val] = @table[params[:start]] unless params.has_key? :start_val
    params[:end_val] = @table[params[:end]] unless params.has_key? :end_val

    return false unless params[:start].between? 0, 26
    return false unless params[:end].between? 1, 27
    return false unless params[:start_val].between? 0, 255
    return false unless params[:end_val].between? 0, 255
    return false unless params[:start_val] <= params[:end_val]

    slope = ( params[:end_val].to_f - params[:start_val]) / ( params[:end] - params[:start] )

    (params[:start]..params[:end]).each do |step|
      @table[step] = (params[:start_val].to_f + (slope * step)).floor
    end

    true
  end

  def read_curve( program_track )
    raise 'Programming track not connected' unless program_track.is_connected?

    TOTAL_STEPS.times do |step|
      value = program_track.read_cv (step + CV_OFFSET )
      p "Read value #{value} for step #{step}" if DEBUG
      @table[step] = value
    end
  end

  # Class methods
  def self.step_to_MPH( step )
    step.to_f * 4.5
  end

  def self.MPH_to_step( mph )
    (mph.to_f / 4.5).ceil
  end

  def self.step_cv( step )
    step + CV_OFFSET
  end
    
end
