##########
# Name: decoder_programmer.rb
# Author: Charles Rothbart
#
# Written: 2018-02-25
# Updated: 2018-11-01

require_relative 'address.rb'

class DecoderProgrammer

  # Constants
  TYPES = [ :locomotive, :accessory, :signal ]

  # Attributes
  attr :cv_list, :interface
  attr_accessor :type, :address
  
  def initialize( params = {} )
    address_params = {}
    if params[:type] == :loco_short || params[:type] == :loco_long
      if params.has_key?( :address )
        address_params[:type] = params[:type]
        address_params[:address] = params[:address]
      end
      params[:type] = :locomotive
    end

    if params.has_key?( :type )
      if TYPES.include? params[:type]
        @type = params[:type]
      else
        raise "Invalid decoder type #{params[:type]}"
      end
    else
      @type = :locomotive
    end

    cv_list = {}

    interface = (params.has_key?( :interface ) && params[:interface] ) || nil

    @address = Address.new( address_params )
  end

  def cv( number )
    return cv_list[number]
  end

  def cv!( number )
    if cv_list.has_key? number
      return cv[number]
    elsif interface.is_connected?
      cv[number] = interface.read_cv( number )
    else
      raise "Interface is not open to read CV"
    end
  end

  def set_cv( number, value )
    if value.between?(0, 255)
      cv_list[number] = value
    else
      raise "Invalid value for CV"
    end
  end

  def set_cv!( number, value )
    raise "Invalid value for CV" unless value.between?(0, 255)
    if interface.is_connected?
      if @address.value
        interface.write_cv( number, value, address: @address )
      else
        interface.write_cv( number, value )
      end
    else
      raise "Interface is not open to read CV"
    end
    cv_list[number] = value
  end
end

